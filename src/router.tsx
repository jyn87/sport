import { createBrowserRouter, Navigate } from 'react-router-dom'

import Layout from './components/Layout'
import Scores from './components/Scores'
import TeamsTable from './components/TeamsTable'

export default createBrowserRouter(
  [
    {
      path: '/',
      element: <TeamsTable />,
    },
    {
      path: '/table',
      element: <TeamsTable />,
    },
    {
      path: '/score',
      element: <Scores />,
    },
    {
      path: '*',
      element: <Navigate replace to="/" />,
    },
  ].map(({ element, ...rest }) => ({
    ...rest,
    element: <Layout> {element}</Layout>,
  })),
)
