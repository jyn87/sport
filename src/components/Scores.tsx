import { Box, Divider, List, ListItem, ListItemText } from '@mui/material'
import Paper from '@mui/material/Paper'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'

import PlaceBox from './PlaceBox'
import arrowRightCircle from '../svg/arrowRightCircle.svg'
import chevronRight from '../svg/chevronRight.svg'
import englandFlag from '../svg/englandFlag.svg'
import { Score } from '../types'

import './Scores.css'

export default function Scores() {
  const [scores, setScores] = useState<Score[]>([])
  const navigate = useNavigate()

  useEffect(() => {
    const getTeams = async () => {
      try {
        const response = await fetch('https://php74.appgo.pl/sport_api/api/public/api/table')
        const data = await response.json()
        setScores(data)
      } catch (error) {
        console.error('Fetch error:', error)
      }
    }

    getTeams()
  }, [])

  return (
    <>
      <Box className="navigation">
        <Box className="navigation-text">Piłka nożna</Box>
        <img src={chevronRight} alt="chevron right" />
        <Box className="sub-navigation-text">Tabela</Box>
      </Box>

      <Paper className="paper-table" elevation={7}>
        <Box className="table-row">
          <Box className="title">
            <img className="flag" src={englandFlag} alt="england flag" />
            Anglia: Premier League
          </Box>
          <a onClick={() => navigate('/')}>
            <Box className="games-nav">
              Mecze
              <img className="arrow-right" src={arrowRightCircle} alt="arrow right circle" />
            </Box>
          </a>
        </Box>
        <Divider className="divider" />

        <Table>
          <TableHead>
            <TableRow className="">
              <TableCell className="table-headers radius-top padding-left-headers">
                <Box>LP.</Box>
              </TableCell>
              <TableCell className="table-headers padding-left-headers">
                <Box>drużyna</Box>
              </TableCell>
              <TableCell className="table-headers" align="center">
                <Box>M</Box>
              </TableCell>
              <TableCell className="table-headers" align="center">
                <Box>B</Box>
              </TableCell>
              <TableCell className="table-headers" align="center">
                <Box>RB</Box>
              </TableCell>
              <TableCell className="table-headers radius-bottom" align="center">
                <Box>P</Box>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {scores.map((score, index) => (
              <TableRow key={score.team.name}>
                <TableCell>
                  <PlaceBox place={index + 1} />
                </TableCell>
                <TableCell>
                  <Box className="team no-wrap">
                    <img className="club-icon" src={score.team.image} alt={score.team.name} />
                    {score.team.name}
                  </Box>
                </TableCell>
                <TableCell align="center">{score.games}</TableCell>
                <TableCell align="center">
                  {score.goals_scored}:{score.goals_conceded}
                </TableCell>
                <TableCell align="center">{score.goals_ratio}</TableCell>
                <TableCell align="center">
                  <b>{score.points}</b>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>

        <List className="list" dense>
          <ListItem className="legend">
            <PlaceBox small place={1} />
            <ListItemText primary="Awans - Liga Mistrzów (Runda grupowa)" />
          </ListItem>
          <ListItem className="legend">
            <PlaceBox small place={5} />
            <ListItemText primary="Awans - Liga Europy (Runda grupowa)" />
          </ListItem>
          <ListItem className="legend">
            <PlaceBox small place={7} />
            <ListItemText primary="Spadek - Championship" />
          </ListItem>
        </List>
      </Paper>
    </>
  )
}
