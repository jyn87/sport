import { Box, CssBaseline } from '@mui/material'
import { ReactNode } from 'react'

import logo from '../svg/logo.svg'
import './Layout.css'

interface Props {
  children: ReactNode
}

export default function Layout({ children }: Props) {
  return (
    <>
      <CssBaseline />
      <Box className="container">
        <Box className="logo">
          <img src={logo} alt="logo" />
        </Box>
        {children}
      </Box>
    </>
  )
}
