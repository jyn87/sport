import { Box } from '@mui/material'
import './PlaceBox.css'

interface Props {
  place: number
  small?: boolean
}

const getClassByPlace = (place: number) => {
  switch (true) {
    case place >= 1 && place <= 4:
      return 'promotion-ucl'
    case place === 5:
      return 'promotion-uel'
    case place === 6:
      return ''
    case place >= 7:
      return 'drop'
    default:
      return ''
  }
}

export default function PlaceBox({ place, small }: Props) {
  return <Box className={`square ${small ? 'small' : ''} ${getClassByPlace(place ?? 0)}`}> {!small && place} </Box>
}
