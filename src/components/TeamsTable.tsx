import { Box, Divider } from '@mui/material'
import Paper from '@mui/material/Paper'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import { Fragment, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'

import arrowLeft from '../svg/arrowLeft.svg'
import arrowRight from '../svg/arrowRight.svg'
import arrowRightCircle from '../svg/arrowRightCircle.svg'
import englandFlag from '../svg/englandFlag.svg'
import monitor from '../svg/monitor.svg'
import { GameData } from '../types'

import './TeamsTable.css'

export default function TeamsTable() {
  const navigate = useNavigate()
  const [games, setGames] = useState<GameData[]>([])
  const [pages, setPages] = useState<number>(1)
  const [page, setPage] = useState<number>(1)

  useEffect(() => {
    const getGames = async () => {
      try {
        const response = await fetch(
          `https://php74.appgo.pl/sport_api/api/public/api/games?page=${page}&onPage=15&orderDirection=asc&orderBy=round`,
        )
        const data = await response.json()
        setPages(data.pages)
        setGames(data.data)
      } catch (error) {
        console.error('Fetch error:', error)
      }
    }

    getGames()
  }, [page])

  return (
    <>
      <Paper className="paper-table" elevation={3}>
        <Box>
          <button className="btn-text btn-show-rounds">Wszystkie</button>
        </Box>
        <Divider className="divider" />
        <Box className="table-row">
          <Box className="title">
            <img className="flag" src={englandFlag} alt="england flag" />
            Anglia: Premier League
          </Box>
          <a onClick={() => navigate('/score')}>
            <Box className="games-nav">
              Tabela
              <img className="arrow-right" src={arrowRightCircle} alt="arrow right circle" />
            </Box>
          </a>
        </Box>
        <Divider className="divider" />

        <Table>
          {games.map((game, index, arr) => (
            <Fragment key={game.id}>
              {game.round !== arr[index - 1]?.round && (
                <TableHead>
                  <TableRow>
                    <TableCell colSpan={4} className="table-headers radius margin padding-left-headers" size="small">
                      RUNDA {game.round}
                    </TableCell>
                  </TableRow>
                </TableHead>
              )}
              <TableBody>
                <TableRow key={game.id}>
                  <TableCell className="table-row">
                    <Box className="table-body-left">
                      <Box className="table-date no-wrap">{game.date}</Box>
                      <Box className="teams vertical-separator">
                        <Box className="teams-text no-wrap separator-teams">
                          <img
                            className="club-icon"
                            src={game.home_team_object.image}
                            alt={game.home_team_object.name}
                          />
                          {game.home_team}
                        </Box>
                        <Box className="teams-text no-wrap">
                          <img
                            className="club-icon"
                            src={game.away_team_object.image}
                            alt={game.away_team_object.name}
                          />
                          {game.away_team}
                        </Box>
                      </Box>
                    </Box>
                    <Box className="table-body-right ">
                      <Box className="score">
                        <Box>{game.home_score}</Box>
                        <Box> {game.away_score}</Box>
                      </Box>
                      <button className="btn-monitor">
                        <img src={monitor} alt="arrow right circle" />
                      </button>
                      <button className="btn-text btn-details">
                        Szczegóły <img src={arrowRightCircle} alt="arrow right circle" />
                      </button>
                    </Box>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Fragment>
          ))}
        </Table>
      </Paper>

      <Box className="pagination">
        <button disabled={page === 1} className="btn-arrow" onClick={() => setPage(page - 1)}>
          <img src={arrowLeft} alt="arrow left" />
          Wstecz
        </button>
        <button disabled={page === pages} className="btn-arrow" onClick={() => setPage(page + 1)}>
          Dalej
          <img src={arrowRight} alt="arrow right" />
        </button>
      </Box>
    </>
  )
}
