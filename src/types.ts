interface Team {
  id: number
  name: string
  image: string
  created_at: string
  updated_at: string
}

export interface Score {
  team: Team
  points: number
  goals_scored: number
  goals_conceded: number
  goals_ratio: number
  games: number
}

export interface GameData {
  id: number
  round: number
  date: string
  home_team: string
  away_team: string
  home_score: number
  away_score: number
  created_at: string
  updated_at: string
  home_team_object: Team
  away_team_object: Team
}
export interface Game {
  total: number
  page: number
  pages: number
  data: GameData
}
